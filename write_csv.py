import csv
import string

import unicodedata as ud

latin_letters= {}

def is_latin(uchr):
    try: return latin_letters[uchr]
    except KeyError:
         return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))

def only_roman_chars(unistr):
    return all(is_latin(uchr)
           for uchr in unistr
           if uchr.isalpha()) 

# get punctuation characters
exclude = set(string.punctuation)
new_stream = True
#outfile = 
hash_dict = None
infile = open('tweets','r')
with open('tweets.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile,delimiter="\t",quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['date','hashtag','count'])
    for line in infile:
        if "[" in line and new_stream:
            if "TwitterException" not in line:
                #print 'beginning stream: %s' %(line)
                # get date from string
                date = line.split("]")[0][1:]
                #print line.split("]")
                #print "date %s" %(date)
                #stop = raw_input('continue?')
                new_stream = False
                print 'hash_dict'
                print hash_dict

                if hash_dict:
                    for hash in hash_dict:
                        if '\t' not in hash:
                        writer.writerow([date, hash, hash_dict[hash]])
                hash_dict = {}
                #stop = raw_input('continue?')
        if 'closed' in line:

            #print 'end of stream: %s' %(line)
            new_stream = True
        if '@' in line:
            hashtags = []
            tweet = line.split(' ')
            for word in tweet:
                if "#" in word:
                    # if hashtag clump
                    if word.count('#') > 1:
                        new_hash = word.split("#")
                        for hash in new_hash:
                            try:
                                hash = ''.join(ch for ch in hash if ch not in exclude)
                                if hash and only_roman_chars(hash):
                                    print hash
                                    # try to get existing hash key
                                    try:		
                                        hash_dict[hash] = hash_dict[hash] + 1
                                    # if new key
                                    except:
                                        # add to list 
                                        hash_dict[hash] = 1
                            except:
                                pass
                    else:
                        hash = word.strip()[1:]
                        try:
                            hash = ''.join(ch for ch in hash if ch not in exclude)
                            if hash and len(hash) < 20:
                                print hash
                                try:		
                                    hash_dict[hash] = hash_dict[hash] + 1
                                    # if new key
                                except: 
                                    # add to list 
                                    hash_dict[hash] = 1

                        except:
                            pass
                    pause = raw_input('pause')
